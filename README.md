This is an old (abandoned) project for visualising spiking neural network behaviour written in C++/OpenGL. I cannot guarantee that the current version of the code works.

The sole purpose of publishing this project is to demonstrate my basic knowledge of OpenGL since I have no other project using OpenGL without any high level wrappers.


Example 1

<img src="/example1.gif"  width="640" height="400">


Example 2

<img src="/example2.gif"  width="640" height="400">
