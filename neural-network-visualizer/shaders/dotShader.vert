#version 330 core

layout (location = 0) in vec2 position;
layout (location = 1) in float brightness;

out vec3 dotColor;

uniform float radius;
uniform mat4 projection;

void main()
{
    gl_Position = projection * vec4(position, 0.0f, 1.0f);
    gl_PointSize = radius * 2 * 2; // Size is diameter, needs to be multiplied by 2 (probably mac thing)
    
    
    if (brightness > 0)
        dotColor = vec3(0.2f, 1.0f, 0.5f);
    else
        dotColor = vec3(1.0f, 0.5f, 0.0f);
}
