#version 330 core

out vec4 FragColor;

uniform float brightness;

void main()
{
    vec4 lineColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);//vec4(vec3(1.0f, 1.0f, 1.0f) * brightness, 1.0f);
    FragColor = lineColor;// * brightness;
}
