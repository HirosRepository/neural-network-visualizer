#version 330 core

in  vec4 dotColor;
out vec4 color;

void main()
{
    color = dotColor;
    
    // Smooth Out the Dot
    vec2 circCoord = 2.0f * gl_PointCoord - 1.0f;   // FIXME: How does this work?
    if (dot(circCoord, circCoord) > 1.0f)
        discard;
}
