#include "include/gui.hpp"
#include "include/network.hpp"
#include "include/renderer.hpp"
#include "include/error.hpp"
#include <GLFW/glfw3.h>

int main(int argc, const char * argv[])
{
    // Main GUI object
    Gui gui((char*)"Window Title"); //FIXME: conversion from string to char
    Network network;
    Renderer renderer(gui, network);
    
    // Main Loop
    while (!glfwWindowShouldClose(gui.getWindow()))
    {
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // FIXME: abstract this out
        glClear(GL_COLOR_BUFFER_BIT);
        // Calculate DeltaTime
        
        // Check for User Inputs
        glfwPollEvents();
        
        // Manage User Inputs
        
        // Update State
        
        // Render
        renderer.drawAxons();
        
        glfwSwapBuffers(gui.getWindow());
    }
    
    // Delete All Resources
    
    // Terminate
    glfwTerminate();
    return 0;
}
