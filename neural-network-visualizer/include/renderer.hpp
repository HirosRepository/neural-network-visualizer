//
//  renderer.hpp
//  neural-network-visualizer
//
//  Created by Hiroyoshi Yamasaki on 01/05/2019.
//  Copyright © 2019 Hiroyoshi Yamasaki. All rights reserved.
//

#ifndef RENDERER_HPP
#define RENDERER_HPP

#include "network.hpp"
#include "shader.hpp"
#include "locations.hpp"
#include "buffer.hpp"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <vector>

// Renderer Class ----------------------------------------------------------------------------------------- //
// This class renders neural network
// -------------------------------------------------------------------------------------------------------- //

class Renderer
{
public:
    // Constructors --------------------------------------------------------------------------------------- //
    Renderer(Gui &gui,                                  // main GUI object
             Network &network);                         // network to be displayed
    
    // Getters -------------------------------------------------------------------------------------------- //
    // TODO: Getters
    
    // Setters -------------------------------------------------------------------------------------------- //
    // TODO: Setters
    
    // Other Member Functions ----------------------------------------------------------------------------- //
    
    // De-allocate All Loaded Resources
    GLvoid clear();
    
    // Draw Functions
    GLvoid drawDots();
    GLvoid drawNeurons();
    GLvoid drawAxons();
    GLvoid drawNetwork();
    GLvoid drawSpikes();
    
    // Update the Current State
    GLvoid update(); //FIXME: is this needed?
    
private:
    // State Variables ------------------------------------------------------------------------------------ //
    
    // Network
    Network *network;

    // Locations
    Locations locations;
    
    // Shaders
    Shader dotShader;
    Shader lineShader;
    Shader neuronShader;
    
    // Buffers
    Buffer buffer;
    
    // Projection Matrix
    glm::mat4 projection;
};

#endif /* RENDERER_HPP */
