#ifndef GUI_HPP
#define GUI_HPP

#include <GL/glew.h>
#include <GLFW/glfw3.h>

// GUI Class ---------------------------------------------------------------------------------------------- //
// This class creates the window using GLFW and GLEW and also handles all the user input
// -------------------------------------------------------------------------------------------------------- //

class Gui
{
public:
    // Constructors --------------------------------------------------------------------------------------- //
    Gui(GLchar *name,                               // name of the window
        GLuint width = 800, GLuint height = 600,    // window size
        GLuint major = 3, GLuint minor = 3,         // version info
        GLboolean resizable = false);               // resizability FIXME: false or GL_FALSE
    
    // Getters -------------------------------------------------------------------------------------------- //
    // TODO: Getters
    GLuint getWidth();
    GLuint getHeight();
    GLFWwindow* getWindow();
    
    // Setters -------------------------------------------------------------------------------------------- //
    // TODO: Set Configurations
    
    // Other Member Functions ----------------------------------------------------------------------------- //
    GLvoid updateWindow();
    
private:
    // State Variables ------------------------------------------------------------------------------------ //
    GLuint mWidth;
    GLuint mHeight;
    GLFWwindow* mWindow;
    
    
    // Member Functions ----------------------------------------------------------------------------------- //
    void keyCallback(GLFWwindow *window, int key, int scanmode, int action, int mode); //FIXME int or GLint?
    
};

#endif /* GUI_HPP */
