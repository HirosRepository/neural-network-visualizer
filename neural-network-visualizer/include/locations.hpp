#ifndef LOCATIONS_HPP
#define LOCATIONS_HPP

#include "network.hpp"
#include <GL/glew.h>

#include <vector>
#include <string>


// Locations Class ---------------------------------------------------------------------------------------- //
// This class calculates the locations of neurons and axons from network shape and stores them
// -------------------------------------------------------------------------------------------------------- //

class Locations
{
public:
    // Constructor ---------------------------------------------------------------------------------------- //
    Locations() {};
    
    // Initializer ---------------------------------------------------------------------------------------- //
    GLvoid init(GLuint width, GLuint height,
                GLuint numSensory, GLuint numInter, GLuint numMotor,
                const std::vector<Axon> *axons,
                GLuint margin = 30);
    
    // Getters -------------------------------------------------------------------------------------------- //
    const std::vector<GLfloat>* getNeuronLocations();
    const std::vector<GLfloat>* getAxonLocations();
    GLvoid   getNeuronData(std::vector<GLfloat> &neurons);
    GLuint   getNumNeurons();
    GLuint   getNumAxons();
    GLfloat  getRadius();
    
    std::vector<GLfloat> calcSpikeLocations(const std::vector<Spike> *spikes);
    
    GLvoid setNeuronBrightness(std::vector<GLfloat> brightness);
private:
    
    // State Variables ------------------------------------------------------------------------------------ //
    
    // Window Size
    GLuint mWidth;
    GLuint mHeight;
    
    // Display Options
    GLuint mMarginSize;     // size of the margin in pixels
    
    // Neuron Size
    GLfloat mRadius;
    
    // Network Shape
    GLuint mNumSensory;     // number of sensory neurons FIXME: what if types are different
    GLuint mNumInter;       // number of interneurons
    GLuint mNumMotor;       // number of motor neurons
    GLuint mNumNeurons;     // number of all neurons
    GLuint mNumAxons;
    
    // Neuron Brightness
    std::vector<GLfloat> mNeuronBrightness;
    
    // Axon Brightness
    std::vector<GLfloat> mAxonBrightness; //FIXME: how am I going to use this?
    
    std::vector<Axon> mAxons;
    
    // Nework Weights
    
    // Neuron Locations
    std::vector<GLfloat> mNeuronLocations; // FIXME: this is ugly, use typedef struct Location
    
    // Axon Locations
    std::vector<GLfloat> mAxonLocations; // FIXME: this is also ugly, use typedef instead
    
    // Recurrent Connections
    std::vector<bool> mIsRecurrent;
    
    std::vector<GLuint> mAxonId2FirstXcoordIdx;
    
    std::vector<bool> mIsDown;
    
    // Member Functions ----------------------------------------------------------------------------------- //
    
    // Initializers
    GLvoid initNeuronLocations();
    GLvoid initNeuronBrightness();
    GLvoid initAxonLocations(const std::vector<Axon> *axons);
    void   addRecurrentAxon(Axon axon);
    GLvoid initAxonBrightness(const std::vector<Axon> *axons);

    // Updaters
    //GLvoid updateNeuronLocations(); TODO: UPDATE
    //GLvoid updateAxonLocations(); TODO:UPDATE
    GLvoid updateNeuronSize();
    //GLvoid updateAxonBrightness(); TODO:UPDATE
    
    // Helper Functions
    int calcNeuronIndex(int neuronIdx, std::string type);   // calculates index of x-coordinate
    void calcParabolaEquation(GLfloat distance, GLfloat displacement, GLfloat &a, GLfloat &b);
    bool isRecurrent(GLuint preId, GLuint postId);
    
    void calcNonRecurrentSpikeLocations(const Spike spike, std::vector<GLfloat> &spikeLocations);
    void calcRecurrentSpikeLocations   (const Spike spike, std::vector<GLfloat> &spikeLocations);
    
    int calcAxonIndex(int axonId);
};

#endif /* LOCATIONS_HPP */
