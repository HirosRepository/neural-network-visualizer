#ifndef NETWORK_HPP
#define NETWORK_HPP

#include "setting.hpp"

#include <GL/glew.h>

#include <vector>
#include <deque>
#include <chrono> // for seed
#include <random>


// ------------------------------------------------------------------------------------------------------------------ //

// Spike Information
typedef struct Spike
{
    GLuint  axonId;   // ID of the axon spike is at
    GLfloat distance; // between 0.0 (at presynaptic neuron) and 1.0 (at postsynaptic neuron)
} Spike;

// Axon Information
typedef struct Axon
{
    GLuint  axonId;     // ID of axon
    GLuint  preId;      // ID of presynaptic neuron connected to this axon
    GLuint  postId;     // ID of postsynaptic neuron connected to this axon
    GLfloat weight;     // weight of this axon, between 0 and 1, negative means inhibitory
    GLfloat speed;      // conduction speed, between 0 and 1, 1 means instantaneous transmission
} Axon;

// Membrane Potential Constants in mV
const GLfloat THRESHOLD = -50.0f;
const GLfloat RESTING   = -70.0f;


// ------------------------------------------------------------------------------------------------------------------ //

class Network
{
public:
    // Constructor -------------------------------------------------------------------------------------------------- //
    Network() {};
    
    // Getters ------------------------------------------------------------------------------------------------------ //
    
    // Network Shape
    GLuint getNumSensory();
    GLuint getNumInter();
    GLuint getNumMotor();
    GLuint getNumNeurons();
    
    // Axon Information
    const std::vector<Axon>* getAxons();
    
    // Potentials normalized between 0 and 1
    std::vector<GLfloat> getNormalizedPotentials();
    
    // Write spike information as {axonId, distance}
    void getSpikeLocations(std::vector<Spike> *spikeLocations);
    //FIXME:const std::vector<spikeData>* getSpikeLocations();
    
    // Other Interfaces --------------------------------------------------------------------------------------------- //
    
    // Initialize   FIXME: possibly in the constructor?
    void init(GLuint numSensory, GLuint numInter, GLuint numMotor, GLfloat connections = CONNECTIONS);
    
    // Do One Time Step Worth of Calculations
    void step();
    
    // Set membrane potential directly
    void setSensory(GLuint index, GLfloat potential);
    //FIXME:void setSensory(std::vector<GLfloat> *potentials);
    
private:
    // State Variables ---------------------------------------------------------------------------------------------- //
    
    // Number of neurons
    GLuint mNumSensory;
    GLuint mNumInter;
    GLuint mNumMotor;
    GLuint mNumNeurons;
    
    // Membrane Potentials
    std::vector<GLfloat> mPotentials;
    
    // Axons
    std::vector<Axon> mAxons;
    
    // Connection Information
    std::vector<std::vector<GLuint>> mPresynaptic2Axons;
    std::vector<GLuint>              mAxons2Postsynaptic;
    
    // Spikes
    std::deque<std::vector<GLuint>> mSpikes;   // each vector contains spikes at that time step
    
    // Initializers ------------------------------------------------------------------------------------------------- //
    void addConnections();//FIXME: change name
    void initAxons(GLfloat connection, bool recurrent = true, GLfloat inhibition = INHIBITION);
    //void initAxonsLoad();
    void initPotentials();
    void initSpikes();
    
    // Other Member Functions --------------------------------------------------------------------------------------- //
    
    // Generate a set of spikes from neuronId
    void generateSpikes(GLuint neuronId);
    
    // Add single axon
    GLuint addAxon(GLuint axonId, GLuint preId, GLuint postId, GLfloat connections);
    GLuint addAxon(GLuint axonId, GLuint preId, GLuint postId, GLfloat connections, GLfloat inhibition);
    
    // Random Initializers ------------------------------------------------------------------------------------------ //
    bool    bernoulli        (std::default_random_engine& generator, GLfloat p);
    GLfloat randomWeight     (std::default_random_engine& generator,
                              GLfloat mean = WEIGHT_MEAN, GLfloat std = WEIGHT_STD);
    GLfloat randomSpeed      (std::default_random_engine& generator,
                              GLfloat mean = SPEED_MEAN,  GLfloat std = SPEED_STD);
    bool    randomInhibition (std::default_random_engine& generator, GLfloat connections);
};

#endif /* NETWORK_HPP */
