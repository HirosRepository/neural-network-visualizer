#ifndef SETTING_HPP
#define SETTING_HPP

#include <GL/glew.h>

// Network related ---------------------------------------------------------------------------------------- //

// Probability that two neurons will be connected, negative means always
const GLfloat CONNECTIONS = -1.0f;

// Probability of inhibitory connection between two neurons, negative meeans never
const GLfloat INHIBITION = -0.8f; //FIXME: better way of representing this?

// Weight Parameters
const GLfloat WEIGHT_MEAN = 0.8f;
const GLfloat WEIGHT_STD  = 1.0f;

// Conduction Speed Parameters
const GLfloat SPEED_MEAN  = 0.01f;
const GLfloat SPEED_STD   = 0.002f;

#endif /* SETTING_HPP */
