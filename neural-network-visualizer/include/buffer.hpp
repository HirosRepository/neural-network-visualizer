#ifndef BUFFER_HPP
#define BUFFER_HPP

#include <GL/glew.h>

#include <vector>

// Buffer Class ------------------------------------------------------------------------------------------- //
// This class calculates the positions of vertices from the data received from the neural network and
// sends the results to the buffer
// -------------------------------------------------------------------------------------------------------- //

class Buffer
{
public:
    // Constructor ---------------------------------------------------------------------------------------- //
    Buffer() {};
    
    // Getters -------------------------------------------------------------------------------------------- //
    GLuint getDotVAO();
    GLuint getLineVAO();
    GLuint getDynamicDotVAO();
    GLuint getNeuronVAO();
    
    // Setters -------------------------------------------------------------------------------------------- //
    // TODO: Setters
    
    // Other Member Functions ----------------------------------------------------------------------------- //
    GLvoid setupDotBuffer  (const std::vector<GLfloat> *vertices);
    GLvoid setupLineBuffer (const std::vector<GLfloat> *vertices);
    GLvoid setupDynamicDotBuffer(std::vector<GLfloat> *vertices);
    GLvoid setupNeuronBuffer(std::vector<GLfloat> *vertices);

private:
    // State Variables ------------------------------------------------------------------------------------ //
    
    // Vertex Buffer Object
    GLuint mDotVBO;
    GLuint mLineVBO;
    GLuint mDynamicDotVBO;
    GLuint mNeuronVBO;
    
    // Vertex Array Object
    GLuint mDotVAO;
    GLuint mLineVAO;
    GLuint mDynamicDotVAO;
    GLuint mNeuronVAO;
};


#endif /* BUFFER_HPP */
