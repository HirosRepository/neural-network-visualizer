#include "../include/locations.hpp"
#include <algorithm>    // for max to calculate radius
#include <math.h>         // for floor
#include <iostream>

// Number of vertices used to draw single recurrent connection
const int NUM_SEGMENTS = 10;

// Initializer -------------------------------------------------------------------------------------------- //
GLvoid Locations::init(GLuint width, GLuint height,
                       GLuint numSensory, GLuint numInter, GLuint numMotor,
                       const std::vector<Axon> *axons,
                       GLuint margin)
{
    // Set the Window Size
    this->mWidth     = width - 2 * margin;     // available width
    this->mHeight    = height - 2 * margin;     // available height
    
    // Set the Margin Size
    this->mMarginSize = margin;
    
    // Set the Network Shape
    this->mNumSensory = numSensory;
    this->mNumInter   = numInter;
    this->mNumMotor   = numMotor;
    this->mNumNeurons = numSensory + numInter + numMotor;
    mNumAxons = 0;
    
    // Initialize All
    initNeuronLocations();
    initAxonLocations(axons);
    initNeuronBrightness();
    initAxonBrightness(axons);
    updateNeuronSize();
}

// Getters ------------------------------------------------------------------------------------------------ //
const std::vector<GLfloat>* Locations::getNeuronLocations()
{
    return &this->mNeuronLocations;
}

const std::vector<GLfloat>* Locations::getAxonLocations()
{
    return &this->mAxonLocations;
}

GLvoid Locations::getNeuronData(std::vector<GLfloat> &neurons)
{
    int numNeurons = this->mNumSensory + this->mNumInter + this->mNumMotor;
    for (int idx = 0; idx < numNeurons; ++idx)
    {
        neurons.push_back(this->mNeuronLocations[idx * 2]);     // x-coordinate
        neurons.push_back(this->mNeuronLocations[idx * 2 + 1]); // y-coordinate
        neurons.push_back(this->mNeuronBrightness[idx]);        // brightness
    }
}

GLuint Locations::getNumNeurons()
{
    return this->mNumSensory + this->mNumInter + this->mNumMotor;
}

GLuint Locations::getNumAxons()
{
    return this->mNumAxons;
}

GLfloat Locations::getRadius()
{
    return this->mRadius;
}

std::vector<GLfloat> Locations::calcSpikeLocations(const std::vector<Spike> *spikes)
{
    std::vector<GLfloat> spikeLocations;
    
    for (int idx = 0; idx < (*spikes).size(); ++idx)
    {
        // Get Spike Data
        const Spike spike = (*spikes)[idx];
        
        if (!mIsRecurrent[spike.axonId])
        {
            calcNonRecurrentSpikeLocations(spike, spikeLocations);
            spikeLocations.push_back(mAxons[spike.axonId].weight);
        }
        else
        {
            calcRecurrentSpikeLocations(spike, spikeLocations);
            spikeLocations.push_back(mAxons[spike.axonId].weight);
        }
    }
    
    return spikeLocations;
}

GLvoid Locations::setNeuronBrightness(std::vector<GLfloat> brightness)
{
    for (int idx = 0; idx < mNumNeurons; ++idx)
        mNeuronBrightness[idx] = brightness[idx];
}

// Private Functions -------------------------------------------------------------------------------------- //


// Initializers ------------------------------------------------------------------------------------------- //

GLvoid Locations::initNeuronLocations()
{
    // Calculate x-coordinates
    GLfloat xSensory, xInter, xMotor;
    GLfloat stripWidth = static_cast<GLfloat>(this->mWidth / 3 / 2); // divide width into 6 strips
    xSensory = stripWidth * 1 + this->mMarginSize; // sensory neuron layer is at the right edge of first strip
    xInter   = stripWidth * 3 + this->mMarginSize; // interneuron layer is at the right edge of third strip
    xMotor   = stripWidth * 5 + this->mMarginSize; // motor neuron layer is at the right edge of fifth strip
    
    // Calculate y-coordinates
    GLfloat ySensory, yInter, yMotor;
    
    // Sensory Neurons
    GLfloat distance = static_cast<GLfloat>(this->mHeight / (this->mNumSensory + 1));
    for (int idx = 0; idx < this->mNumSensory; ++idx)
    {
        ySensory = distance * (idx + 1) + this->mMarginSize;
        this->mNeuronLocations.push_back(xSensory);
        this->mNeuronLocations.push_back(ySensory);
    }
    
    // Interneurons
    distance = static_cast<GLfloat>(this->mHeight / (this->mNumInter + 1));
    for (int idx = 0; idx < this->mNumInter; ++idx)
    {
        yInter = distance * (idx + 1) + this->mMarginSize;
        this->mNeuronLocations.push_back(xInter);
        this->mNeuronLocations.push_back(yInter);
    }
    
    // Motor neurons
    distance = static_cast<GLfloat>(this->mHeight / (this->mNumMotor + 1));
    for (int idx = 0; idx < this->mNumMotor; ++idx)
    {
        yMotor = distance * (idx + 1) + this->mMarginSize;
        this->mNeuronLocations.push_back(xMotor);
        this->mNeuronLocations.push_back(yMotor);
    }
}

GLvoid Locations::initAxonLocations(const std::vector<Axon> *axons)
{
    for (auto axon : *axons)
    {
        if (!isRecurrent(axon.preId, axon.postId))
        {
            mAxonId2FirstXcoordIdx.push_back(static_cast<GLuint> (mAxonLocations.size()));
            
            // Presynaptic Neuron
            GLfloat xCoord = this->mNeuronLocations[axon.preId * 2];
            GLfloat yCoord = this->mNeuronLocations[axon.preId * 2 + 1];
            this->mAxonLocations.push_back(xCoord);
            this->mAxonLocations.push_back(yCoord);
            
            // Postsynaptic Neuron
            xCoord = this->mNeuronLocations[axon.postId * 2];
            yCoord = this->mNeuronLocations[axon.postId * 2 + 1];
            this->mAxonLocations.push_back(xCoord);
            this->mAxonLocations.push_back(yCoord);
            
            // Update Number of axons
            this->mNumAxons++;
            
            // Is not recurrent
            mIsRecurrent.push_back(false);
            mIsDown.push_back(false);
        }
        else
        {
            addRecurrentAxon(axon);
            mNumAxons += NUM_SEGMENTS;//FIXME: why did I have NUM_SEGMENTS * 2;
            mIsRecurrent.push_back(true);
            
            if (axon.preId < axon.postId)
                mIsDown.push_back(true);
            else
                mIsDown.push_back(false);
        }
    }
}

void Locations::addRecurrentAxon(Axon axon)
{
    // Find out which one is higher up
    GLfloat preY  = mNeuronLocations[axon.preId  * 2 + 1];  // y-coordinate of presynaptic neuron
    GLfloat postY = mNeuronLocations[axon.postId * 2 + 1]; // y-coordinate of postsynaptic neuron
    GLfloat higherY = (preY < postY) ? preY : postY;   // higher means smaller y-coordinate value
    
    // Calculate the distance between the two neurons
    GLfloat distance = abs(preY - postY);
    
    // Calculate the quadratic equation
    GLfloat a, b;
    GLfloat displacement = 50;// mWidth * 0.01f; //FIXME: calculate horizontal displacement
    calcParabolaEquation(distance, displacement, a, b);
    
    // Calculate coordinates
    GLfloat xCoord = mNeuronLocations[axon.preId * 2];  // x-coordinate of the interneuron layer
    GLfloat yCoord = higherY;                       // start from neuron with higher y-coordinate
    GLfloat stride = distance / NUM_SEGMENTS;
    GLfloat xLast = xCoord;
    GLfloat yLast = yCoord;
    
    mAxonId2FirstXcoordIdx.push_back(static_cast<GLuint>(mAxonLocations.size()));
    
    for (int idx = 1; idx <= NUM_SEGMENTS ; ++idx)
    {
        // Calculate the coordinates
        GLfloat y = stride * idx;                // counting from top neuron, where y = 0
        GLfloat x = a * (y * y) + b * y;         // equation: ay^2 + by
    
        
        
        // Push to the location vector
        mAxonLocations.push_back(xLast);
        mAxonLocations.push_back(yLast);
        mAxonLocations.push_back(xCoord + x);
        mAxonLocations.push_back(yCoord + y);
        
        
        
        xLast = xCoord + x;
        yLast = yCoord + y;
    }
}

GLvoid Locations::initNeuronBrightness()
{
    int numNeurons = this->mNumSensory + this->mNumInter + this->mNumMotor;
    
    for (int idx = 0; idx < numNeurons; ++idx)
        this->mNeuronBrightness.push_back(0.0f); // initial brightness is 0.0f
}

GLvoid Locations::initAxonBrightness(const std::vector<Axon> *axons)
{
    for (auto axon : *axons)
    {
        this->mAxonBrightness.push_back(axon.weight);
        mAxons.push_back(axon);
    }
}


// Updaters ----------------------------------------------------------------------------------------------- //

/*// Update Neuron Locations from Network Shape
GLvoid Locations::updateNeuronLocations()
{
    //TODO:updateNeuronLocations
}

// Update Axon Locations from Network Shape
GLvoid Locations::updateAxonLocations()
{
    //TODO: updateAxonlocations
}*/

// Update Neurons Size
GLvoid Locations::updateNeuronSize()
{
    // Get Maximum Number of Neurons in a Layer
    GLuint maxNumNeurons = std::max(this->mNumSensory, this->mNumInter);
    maxNumNeurons = std::max(maxNumNeurons, this->mNumMotor);
    
    // Radius Must Be Smaller Than the Half of Distance Between Neurons
    GLfloat distance = static_cast<GLfloat>(this->mHeight / (maxNumNeurons + 1));
    GLfloat ratio = 0.8f;   // 80% of the distance TODO: make this adjustable
    
    GLfloat maxRadius = 20.0f;
    GLfloat radius = distance / 2 * ratio;
    this->mRadius = (radius > maxRadius) ? maxRadius : radius;
}

/*GLvoid Locations::updateAxonBrightness()
{
    //TODO: assuming that weights are between 0 and 1
}*/


// Helper Functions --------------------------------------------------------------------------------------- //

// Calculates Index of x-coordinate of Given Neuron
int Locations::calcNeuronIndex(int neuronIdx, std::string type)
{
    // Sensory Neurons
    if (type == "SENSORY")
    {
        return neuronIdx * 2;
    }
    
    // Inter Neurons
    else if (type == "INTER")
    {
        return 2 * this->mNumSensory + neuronIdx * 2;
    }
    
    // Motor Neurons
    else
    {
        return 2 * (this->mNumSensory + this->mNumInter) + neuronIdx * 2;
    }
}

void Locations::calcParabolaEquation(GLfloat distance, GLfloat displacement, GLfloat &a, GLfloat &b)
{
    // distance is distance between two neurons
    // displacement is horizontal distance from the interneuron layer
    // equation is: a * x^2 + b * x = y
    a = -4 * displacement / (distance * distance);
    b = 4 * displacement / distance;
}

bool Locations::isRecurrent(GLuint preId, GLuint postId)
{
    bool pre  = (mNumSensory <= preId)  && (preId  < mNumSensory + mNumInter);
    bool post = (mNumSensory <= postId) && (postId < mNumSensory + mNumInter);
    return pre && post;
}


void Locations::calcNonRecurrentSpikeLocations(const Spike spike, std::vector<GLfloat> &spikeLocations)
{
    // Difference Vector
    GLfloat xDiff, yDiff;
    
    // Calculate Difference Vector
    GLfloat x1 = this->mAxonLocations[mAxonId2FirstXcoordIdx[spike.axonId]];
    GLfloat y1 = this->mAxonLocations[mAxonId2FirstXcoordIdx[spike.axonId] + 1];
    GLfloat x2 = this->mAxonLocations[mAxonId2FirstXcoordIdx[spike.axonId] + 2];
    GLfloat y2 = this->mAxonLocations[mAxonId2FirstXcoordIdx[spike.axonId] + 3];
    xDiff = x2 - x1;
    yDiff = y2 - y1;
    
    // Calculate Distance Travelled
    xDiff *= spike.distance;
    yDiff *= spike.distance;
    
    // Push to the Vector
    spikeLocations.push_back(x1 + xDiff);
    spikeLocations.push_back(y1 + yDiff);
}


void Locations::calcRecurrentSpikeLocations(const Spike spike, std::vector<GLfloat> &spikeLocations)
{
    // Calculate horizontal distance travelled
    GLfloat y1 = mAxonLocations[mAxonId2FirstXcoordIdx[spike.axonId] + 1];
    GLfloat y2 = mAxonLocations[mAxonId2FirstXcoordIdx[spike.axonId] + NUM_SEGMENTS * 4 - 1];
    GLfloat yDiff = y2 - y1;
    
    // Determine which segment the spike is on
    GLfloat segmentLength = yDiff / NUM_SEGMENTS;
    GLfloat segmentLocation = spike.distance * yDiff;
    GLuint segmentIdx = static_cast<GLuint>(floor(segmentLocation / segmentLength));
    
    // Calculate the coordinate of the segment
    GLfloat x1Vertex = mAxonLocations[mAxonId2FirstXcoordIdx[spike.axonId] + segmentIdx * 4];
    GLfloat y1Vertex = mAxonLocations[mAxonId2FirstXcoordIdx[spike.axonId] + segmentIdx * 4 + 1];
    GLfloat x2Vertex = mAxonLocations[mAxonId2FirstXcoordIdx[spike.axonId] + segmentIdx * 4 + 2];
    GLfloat y2Vertex = mAxonLocations[mAxonId2FirstXcoordIdx[spike.axonId] + segmentIdx * 4 + 3];
    
    // Calculate segment slope
    GLfloat slope = (x2Vertex - x1Vertex) / (y2Vertex - y1Vertex);
    
    // Calculate how far on the segment it has travelled
    GLfloat xDistance = (segmentLocation - segmentIdx * segmentLength) * slope;
    
    // Push to the vector
    if (mIsDown[spike.axonId])
    {
        spikeLocations.push_back(x1Vertex + xDistance);
        spikeLocations.push_back(y1 + segmentLocation);
    }
    else
    {
        spikeLocations.push_back(x1Vertex + xDistance);
        spikeLocations.push_back(y2 - segmentLocation);
    }
}
