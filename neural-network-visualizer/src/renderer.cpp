#include "../include/gui.hpp"
#include "../include/shader.hpp"
#include "../include/renderer.hpp"
#include "../include/network.hpp"
#include "../include/locations.hpp"


#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

Renderer::Renderer(Gui &gui,
                   Network &network)
{
    // Initialize Locations ------------------------------------------------------------------------------- //
    this->locations.init(gui.getWidth(), gui.getHeight(),
                         network.getNumSensory(), network.getNumInter(), network.getNumMotor(),
                         network.getAxons());
                         //network.getSensory2InterWeights(), network.getInter2MotorWeights());
    
    // Copy Pointers -------------------------------------------------------------------------------------- //
    this->network = &network;
    
    // Configure Shaders ---------------------------------------------------------------------------------- //
    
    // Load the Shaders
    this->dotShader.loadShader("shaders/dotShader.vert", "shaders/dotShader.frag");
    this->lineShader.loadShader("shaders/lineShader.vert", "shaders/lineShader.frag");
    this->neuronShader.loadShader("shaders/neuronShader.vert", "shaders/neuronShader.frag");
    
    // Set Projection Matrix
    this->projection = glm::ortho(0.0f,                                      // left
                                  static_cast<GLfloat>(gui.getWidth()),      // right
                                  static_cast<GLfloat>(gui.getHeight()),     // bottom
                                  0.0f,                                      // top
                                  -1.0f,                                     // zNear
                                  1.0f);                                     // zFar
    this->dotShader.use();
    this->dotShader.setUniformMat4("projection", this->projection);
    this->lineShader.use();
    this->lineShader.setUniformMat4("projection", this->projection);
    this->neuronShader.use();
    this->neuronShader.setUniformMat4("projection", this->projection);
    
    // Set Up Buffers ------------------------------------------------------------------------------------- //
    this->buffer.setupDotBuffer  (this->locations.getNeuronLocations());
    this->buffer.setupLineBuffer (this->locations.getAxonLocations());
}

GLvoid Renderer::update()
{
    this->locations.setNeuronBrightness(this->network->getNormalizedPotentials());
}
    
    
// Getters ------------------------------------------------------------------------------------------------ //
// TODO: Getters
    
// Setters ------------------------------------------------------------------------------------------------ //
// TODO: Setters
    
// Draw Functions ----------------------------------------------------------------------------------------- //

// Draw just the Neurons
GLvoid Renderer::drawDots()
{
    // Use dotShader
    this->dotShader.use();
    this->dotShader.setUniformFloat("radius", this->locations.getRadius());//FIXME: change radius
    
    
    // Draw Dots
    glEnable(GL_PROGRAM_POINT_SIZE);
    glBindVertexArray(this->buffer.getDotVAO());
    glDrawArrays(GL_POINTS, 0, this->locations.getNumNeurons());
    glBindVertexArray(0);
    glDisable(GL_PROGRAM_POINT_SIZE);
}

// Draw the Spikes
GLvoid Renderer::drawNeurons()
{
    // Use dotShader
    this->neuronShader.use();
    this->neuronShader.setUniformFloat("radius", this->locations.getRadius()); // FIXME: make this adjustable
    
    // Fetch Data
    std::vector<GLfloat> neurons;
    this->locations.getNeuronData(neurons);
    
    // Set Up Buffer
    this->buffer.setupNeuronBuffer(&neurons);
    
    // Draw Dots
    glEnable(GL_PROGRAM_POINT_SIZE);
    glBindVertexArray(this->buffer.getNeuronVAO());
    glDrawArrays(GL_POINTS, 0, this->locations.getNumNeurons());
    glBindVertexArray(0);
    glDisable(GL_PROGRAM_POINT_SIZE);
}

// Draws just the Axons
GLvoid Renderer::drawAxons()
{
    // Use lineShader
    this->lineShader.use();
    //this->lineShader.setUniformFloat("brightness", <#GLfloat value#>)("brightness", );
    
    // Draw Lines
    glBindVertexArray(this->buffer.getLineVAO());
    glDrawArrays(GL_LINES, 0, this->locations.getNumAxons() * 2);   // 2 vertices per axon
    glBindVertexArray(0);
}

// Draws the Entire Network
GLvoid Renderer::drawNetwork()
{
    
}

// Draw the Spikes
GLvoid Renderer::drawSpikes()
{
    // Use dotShader
    this->dotShader.use();
    this->dotShader.setUniformFloat("radius", 5.0f); // FIXME: make this adjustable
    
    // Fetch Data
    std::vector<Spike> spikeLocations;
    this->network->getSpikeLocations(&spikeLocations);
    std::vector<GLfloat> spikes = this->locations.calcSpikeLocations(&spikeLocations);
    
    // Set Up Buffer
    this->buffer.setupDynamicDotBuffer(&spikes);
    
    // Draw Dots
    glEnable(GL_PROGRAM_POINT_SIZE);
    glBindVertexArray(this->buffer.getDynamicDotVAO());
    glDrawArrays(GL_POINTS, 0, static_cast<GLuint>(spikeLocations.size()));
    glBindVertexArray(0);
    glDisable(GL_PROGRAM_POINT_SIZE);
}
