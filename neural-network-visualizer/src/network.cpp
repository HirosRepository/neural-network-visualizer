#include "network.hpp"

#include <random>
#include <iostream>

int seed = static_cast<int>(std::chrono::system_clock::now().time_since_epoch().count());
std::default_random_engine generator(1);

// Getters ---------------------------------------------------------------------------------------------------------- //
GLuint Network::getNumSensory()
{
    return mNumSensory;
}

GLuint Network::getNumInter()
{
    return mNumInter;
}

GLuint Network::getNumMotor()
{
    return mNumMotor;
}

GLuint Network::getNumNeurons()
{
    return mNumNeurons;
}

const std::vector<Axon>* Network::getAxons()
{
    return &mAxons;
}

std::vector<GLfloat> Network::getNormalizedPotentials() // FIXME: passing by value is probably too inefficient
{
    std::vector<GLfloat> normalized;
    
    for (int idx = 0; idx < mNumNeurons; ++idx)
        normalized.push_back((mPotentials[idx] - RESTING) / (THRESHOLD - RESTING));
    return normalized;
}

void Network::getSpikeLocations(std::vector<Spike> *spikeLocations)
{
    for (int step = 0; step < mSpikes.size(); ++step)
    {
        for (int idx = 0; idx < mSpikes[step].size(); ++idx)
        {
            // Axon the spike is at
            Axon axon = mAxons[mSpikes[step][idx]];
            
            // Distance left to travel
            GLfloat toTravel = axon.speed * (step + 1);
            
            // Spike Information
            Spike spike = { axon.axonId, 1.0f - toTravel };
            (*spikeLocations).push_back(spike);
        }
    }
}


// Initializers --------------------------------------..............------------------------------------------------- //

void Network::addConnections()//FIXME: change name
{
    // add empty vectors
    for (int idx = 0; idx < mNumSensory + mNumInter; ++idx)
    {
        std::vector<GLuint> tmp;
        mPresynaptic2Axons.push_back(tmp);
    }
}

void Network::init(GLuint numSensory, GLuint numInter, GLuint numMotor, GLfloat connections)
{
    // Initialize Numbers of Neurons
    mNumSensory = numSensory;
    mNumInter   = numInter;
    mNumMotor   = numMotor;
    mNumNeurons = numSensory + numInter + numMotor;
    
    // Calling Initializers
    addConnections();
    initAxons(connections); // probability of two neuron being connected FIXME: why pass here?
    initPotentials();
}

// Do One Time Step Worth of Calculations
void Network::step()
{
    if (mSpikes.size() == 0)
        return;
    
    std::vector<GLuint>& spikes = mSpikes[0];
    
    
    // Increment Potentials and Generate Spikes
    for (auto& axonId : spikes)
    {
        // Reference to the Neuron
        GLfloat& neuronPotential = mPotentials[mAxons2Postsynaptic[axonId]];
        
        // Increment Potential
        neuronPotential += mAxons[axonId].weight;//FIXME: multiply by some constant
        // Check if Theashold is Reached
        if (neuronPotential >= THRESHOLD)
        {
            // Generate Spikes
            if (mAxons2Postsynaptic[axonId] < (mNumSensory + mNumInter)) // motor neurons don’t need to send signals
                generateSpikes(mAxons[axonId].postId);  // Neuron ID of the postsynaptic neuron
            
            // Reset the Potential
            mPotentials[mAxons2Postsynaptic[axonId]] = RESTING;
        }
    }
    
    // Remove Used Spikes
    mSpikes.pop_front();
}

/*void Network::setSensory(std::vector<GLfloat> *potentials)
{
    for (int idx = 0; idx < mNumSensory; ++idx)
    {
        mPotentials[idx] = (*potentials)[idx];
        
        if (mPotentials[idx] >= THRESHOLD && idx < (mNumSensory + mNumInter))
        {
            generateSpikes(idx);
            mPotentials[idx] = RESTING;
        }
    }
}*/

void Network::setSensory(GLuint index, GLfloat potential)
{
    mPotentials[index] = potential;
    if (mPotentials[index] >= THRESHOLD)
    {
        generateSpikes(index);
        //mPotentials[index] = RESTING;
    }
}


// Initializers ----------------------------------------------------------------------------------------------------- //
void Network::initAxons(GLfloat connections, bool recurrent, GLfloat inhibition)
{
    GLuint axonId = 0;
    
    // Sensory Layer to Interneuron layer
    for (GLuint sensory = 0; sensory < mNumSensory; ++sensory)
    {
        for (GLuint inter = 0; inter < mNumInter; ++inter)
        {
            // Add excitatory axon
            axonId = addAxon(axonId, sensory, mNumSensory + inter, connections);
            
            // Add inhibitory axon
            axonId = addAxon(axonId, sensory, mNumSensory + inter, connections, inhibition);
        }
    }
    
    // Recurrent Connections
    if (recurrent)
    {
        for (GLuint pre = 0; pre < mNumInter; ++pre)
        {
            for(GLuint post = 0; post < mNumInter; ++post)
            {
                // No connection onto itself
                if (pre == post)
                    continue;
                
                // Add exitatory axon
                axonId = addAxon(axonId, mNumSensory + pre, mNumSensory + post, connections);

                // Add inhibitory axon
                axonId = addAxon(axonId, mNumSensory + pre, mNumSensory + post, connections, inhibition);
            }
        }
    }

    // Interneuron Layer to Motorneuron layer
    for (GLuint inter = 0; inter < mNumInter; ++inter)
    {
        for (GLuint motor = 0; motor < mNumMotor; ++motor)
        {
            
            axonId = addAxon(axonId, mNumSensory + inter, mNumSensory + mNumInter + motor, connections);

            axonId = addAxon(axonId, mNumSensory + inter, mNumSensory + mNumInter + motor, connections, inhibition);
        }
    }
}

void Network::initPotentials()
{
    // Push Resting Potential for NumNeurons times
    for (int idx = 0; idx < mNumNeurons; ++idx)
    {
        mPotentials.push_back(RESTING);
    }
}

// Generate Spikes from neuronId (Presynapic neuron)
void Network::generateSpikes(GLuint neuronId)
{
    // If motor neuron, end
    if (neuronId > mNumSensory + mNumInter)
        return;
    
    // Get Axons connected to the presynaptic neuron
    const std::vector<GLuint> axons = mPresynaptic2Axons[neuronId];
    
    // No axon is connected, end
    if (axons.size() == 0)
        return;
    
    // Generate spikes
    for (GLuint idx = 0; idx < axons.size(); ++idx)
    {
        // Number of time steps needed to reach postsynaptic neuron
        int steps = static_cast<int>(1.0f / mAxons[axons[idx]].speed);
        
        // Make sure enough vectors exist in deque
        while (steps + 1 > mSpikes.size())
        {
            std::vector<GLuint> tmp;
            mSpikes.push_back(tmp);
        }
        mSpikes[steps].push_back(axons[idx]);
    }
}

// Random Initializers ---------------------------------------------------------------------------------------------- //

GLfloat Network::randomWeight(std::default_random_engine& generator, GLfloat mean, GLfloat std)
{
    GLfloat weight;
    std::normal_distribution<GLfloat> normal(mean, std);
    
    do
    {
        weight = normal(generator);
    } while (weight < 0.0f || weight > 1.0f);
    
    return weight;
}

GLfloat Network::randomSpeed(std::default_random_engine& generator, GLfloat mean, GLfloat std)
{
    GLfloat speed;
    std::normal_distribution<GLfloat> normal(mean, std);
    
    do
    {
        speed = normal(generator);
    } while (speed < 0.0f || speed > 1.0f);
    
    return speed;
}

bool Network::bernoulli(std::default_random_engine &generator, GLfloat p)
{
    std::bernoulli_distribution ber(p);
    
    return ber(generator);
}

bool Network::randomInhibition(std::default_random_engine &generator, GLfloat connections)
{
    // Negative value means never connect inhibitory
    if (connections < 0)
        return false;
    else
        return bernoulli(generator, connections);
}

GLuint Network::addAxon(GLuint axonId, GLuint preId, GLuint postId, GLfloat connections)
{
    if ((connections < 0 || bernoulli(generator, connections)))
    {
        Axon axon =
        {
            axonId,
            preId,
            postId,
            randomWeight(generator),
            randomSpeed(generator),
        };
        
        mPresynaptic2Axons[preId].push_back(axonId);
        mAxons2Postsynaptic.push_back(postId);
        mAxons.push_back(axon);
        axonId++;
    }
    return axonId;
}


GLuint Network::addAxon(GLuint axonId, GLuint preId, GLuint postId, GLfloat connections, GLfloat inhibition)
{
    if ((connections < 0 || bernoulli(generator, connections)) && randomInhibition(generator, inhibition))
    {
        Axon axon =
        {
            axonId,
            preId,
            postId,
            randomWeight(generator) * -1,
            randomSpeed(generator),
        };
        
        mPresynaptic2Axons[preId].push_back(axonId);
        mAxons2Postsynaptic.push_back(postId);
        mAxons.push_back(axon);
        axonId++;
    }
    return axonId;
}
