#include "../include/gui.hpp"


// Constructors ------------------------------------------------------------------------------------------- //
Gui::Gui(GLchar *name, GLuint width, GLuint height, GLuint major, GLuint minor, GLboolean resizable)
{
    // Set Window Size
    this->mWidth    = width;
    this->mHeight   = height;
    
    // Initialize GLFW
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, major);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minor);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // needed for Mac
    glfwWindowHint(GLFW_RESIZABLE, resizable);
    
    // Create window
    this->mWindow = glfwCreateWindow(this->mWidth, this->mHeight, name, nullptr, nullptr);
    glfwMakeContextCurrent(this->mWindow);
    
    // Initialize GLEW
    glewExperimental = GL_TRUE;
    glewInit();
    glGetError();
    
    // TODO: Set up KeyCallbacks
    //glfwSetKeyCallback(this->window, this->keyCallback); //FIXME: how to do this?
    
    // TODO: OpenGL Configurations
    glViewport(0, 0, this->mWidth, this->mHeight);
}

// Getters ------------------------------------------------------------------------------------------------ //
GLuint Gui::getWidth()
{
    return this->mWidth;
}

GLuint Gui::getHeight()
{
    return this->mHeight;
}

GLFWwindow* Gui::getWindow()
{
    return this->mWindow;
}

// Setters ------------------------------------------------------------------------------------------------ //

// Member Functions --------------------------------------------------------------------------------------- //
void Gui::keyCallback(GLFWwindow *window, int key, int scanmode, int action, int mode)
{
    // Press esc to Close
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
}
