#include "../include/buffer.hpp"

// Getters -------------------------------------------------------------------------------------------- //
GLuint Buffer::getDotVAO()
{
    return mDotVAO;
}

GLuint Buffer::getLineVAO()
{
    return mLineVAO;
}

GLuint Buffer::getDynamicDotVAO()
{
    return mDynamicDotVAO;
}

GLuint Buffer::getNeuronVAO()
{
    return mNeuronVAO;
}

// Member Functions ----------------------------------------------------------------------------------- //
GLvoid Buffer::setupDotBuffer(const std::vector<GLfloat> *vertices)
{
    // Generate Buffers
    glGenVertexArrays(1, &this->mDotVAO);
    glGenBuffers(1, &this->mDotVBO);
    
    // Bind VBO
    glBindBuffer(GL_ARRAY_BUFFER, this->mDotVBO);
    glBufferData(GL_ARRAY_BUFFER,
                 (*vertices).size() * sizeof(GLfloat),
                 &(*vertices)[0],
                 GL_STATIC_DRAW);
    
    // Bind VAO
    glBindVertexArray(this->mDotVAO);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0,                        // index: at the beginning
                          2,                        // size: 2, in vec2
                          GL_FLOAT,                 // type
                          GL_FALSE,                 // normalized
                          2 * sizeof(GLfloat),      // stride
                          (GLvoid*) 0);             // pointer: offset 0
    
    // Unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

GLvoid Buffer::setupLineBuffer(const std::vector<GLfloat> *vertices)
{
    // Generate Buffers
    glGenVertexArrays(1, &this->mLineVAO);
    glGenBuffers(1, &this->mLineVBO);
    
    // Bind VBO
    glBindBuffer(GL_ARRAY_BUFFER, this->mLineVBO);
    glBufferData(GL_ARRAY_BUFFER,
                 (*vertices).size() * sizeof(GLfloat),
                 &(*vertices)[0],
                 GL_STATIC_DRAW);
    
    // Bind VAO
    glBindVertexArray(this->mLineVAO);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0,                        // index: at the beginning
                          2,                        // size: 2, in vec2
                          GL_FLOAT,                 // type
                          GL_FALSE,                 // normalized
                          2 * sizeof(GLfloat),      // stride
                          (GLvoid*) 0);             // pointer: offset 0
    
    // Unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

GLvoid Buffer::setupDynamicDotBuffer(std::vector<GLfloat> *vertices)
{
    // Generate Buffers
    glGenVertexArrays(1, &this->mDynamicDotVAO);
    glGenBuffers(1, &this->mDynamicDotVBO);
    
    // Bind VBO
    glBindBuffer(GL_ARRAY_BUFFER, this->mDynamicDotVBO);
    glBufferData(GL_ARRAY_BUFFER,
                 (*vertices).size() * sizeof(GLfloat),
                 &(*vertices)[0],
                 GL_DYNAMIC_DRAW);
    
    // Bind VAO
    glBindVertexArray(this->mDynamicDotVAO);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0,                        // index: at the beginning
                          2,                        // size: 2, in vec2
                          GL_FLOAT,                 // type
                          GL_FALSE,                 // normalized
                          3 * sizeof(GLfloat),      // stride
                          (GLvoid*) 0);             // pointer: offset 0
    
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1,                                       // index: location 1
                          1,                                       // size: 1, in float
                          GL_FLOAT,                                // type
                          GL_FALSE,                                // normalized
                          3 * sizeof(GLfloat),                     // stride
                          (GLvoid*) (2 * sizeof(GLfloat)));        // pointer: offset after 2 floats
    
    // Unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

GLvoid Buffer::setupNeuronBuffer(std::vector<GLfloat> *vertices)
{
    // Generate Buffers
    glGenVertexArrays(1, &this->mNeuronVAO);
    glGenBuffers(1, &this->mNeuronVBO);
    
    // Bind VBO
    glBindBuffer(GL_ARRAY_BUFFER, this->mNeuronVBO);
    glBufferData(GL_ARRAY_BUFFER,
                 (*vertices).size() * sizeof(GLfloat),
                 &(*vertices)[0],
                 GL_DYNAMIC_DRAW);
    
    // Bind VAO
    glBindVertexArray(this->mNeuronVAO);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0,                        // index: location 0
                          2,                        // size: 3, in vec2
                          GL_FLOAT,                 // type
                          GL_FALSE,                 // normalized
                          3 * sizeof(GLfloat),      // stride
                          (GLvoid*) 0);             // pointer: offset 0
    
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1,                                       // index: location 1
                          1,                                       // size: 1, in float
                          GL_FLOAT,                                // type
                          GL_FALSE,                                // normalized
                          3 * sizeof(GLfloat),                     // stride
                          (GLvoid*) (2 * sizeof(GLfloat)));        // pointer: offset after 2 floats
    
    // Unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}
